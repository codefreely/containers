#!/bin/bash

if [ `whoami` != 'root' ]; then
	echo "You are not root. Docker requires root to build containers. Either exit and rerun as root, or use sudo:"
	sudo $0
	exit
fi

docker build -t opensuse-base opensuse-base
docker build -t php7-mariadb-apache-opensuse php7-mariadb-apache-opensuse

