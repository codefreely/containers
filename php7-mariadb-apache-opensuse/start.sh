DATADIR=/opt/data
MYSQL_DATADIR=$DATADIR/db
HTTPD_DATADIR=$DATADIR/http

nohup mysqld_safe --datadir=$MYSQL_DATADIR --user=root &
/usr/sbin/apache2ctl # start apache

# this'll be quick, won't it?
sleep inf

# should never get here, if so the container stops.
