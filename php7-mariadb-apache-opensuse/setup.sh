DATADIR=/opt/data
MYSQL_DATADIR=$DATADIR/db
HTTPD_DATADIR=$DATADIR/http
mkdir -p "$MYSQL_DATADIR"
mkdir -p "$HTTPD_DATADIR"

sed -ibak s,\<webroot\>,$HTTPD_DATADIR, /etc/apache2/default-server.conf

# enable PHP, etc
/usr/sbin/a2enmod php7

# install the mariadb databases
/usr/bin/mysql_install_db --datadir=$MYSQL_DATADIR

